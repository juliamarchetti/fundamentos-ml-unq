# Fundamentos ML UNQ
Repositorio que contiene datos para practicar el contenido visto en el curso **Fundamentos de Machine Learning UNQ (edición-2020)**
## src
La carpeta **src** fue creada para guardar el código allí (por el momento vacía)
## data
En la carpeta **data** van a encontrar un df que pueden utilizar para practicar

Breve descripción de la df: 

Esta tabla contiene información sobre posiciones ordenadas y posiciones desordenadas para proteínas que son instrínsicamente desordenadas. Este df es parte de un trabajo que pueden mirar en https://www.biorxiv.org/content/10.1101/2020.07.29.227363v1. 

El objetivo de este trabajo fue intentar explicar si existe algún tipo de relación entre la información estructural y las velocidades de evolución sitio específicas y el caracter orden-desorden de esos sitios. 

En relación a los visto en el curso se me ocurrió que se pueden pensar esas característias estructurales y evolutivas como features que sirvan para predecir y modelar si una posición va a ser desordenada o no. 

Estos datos son obtenidos de ensembles de NMR donde cada estructura contenida en ese ensemble (conocida como confórmero) es una foto del estado de esa proteína en un momento dado. Los ensembles de NMR en promedio están formados por 20 conformaciones distintas.

La df contiene la siguientes columnas: 
- pdb_chain: identificador PDB de la proteína 
- position: posición secuencial 
- is_disorder_es: predicción si esa posición es ordenada o no (basada en el predictor ESpritz) --> esta sería la variable a modelar
- rmsf: rmsf para esa posición (el rsmf nos da una idea de la 'movilidad' de esa posición en los distintos confórmeros)
- normperprot: velocidad de evolución sitio específica normalizada (por el promedio de la velocidad de toda la proteína)
- mean_contacts: promedio de contactos para esa información (se promedia la información para esa posición para todo el ensemble)
- min_contacts: mínimo número de contactos para esa posición
- max_contacts: máximo número de contactos para esa posición
- contact_permanence: fracción de confórmeros que para esa posición tienen al menos un contactos (i.e: dado un ensemble de 10 confórmeros, si en esa posición 5 están haciendo contactos entonces contact_permanece = 0.5)
- mean_id: porcentaje de identidad promedio para esa posición en el alineamiento que fue utilizado para obtener las velocidades de evolución.
